$ = jQuery;
$(document).ready(function(){
    var hide = null;
   $('#menuBtn').click(function(e){
       $('.simplenav').toggleClass('simplenav-open');
       $(this).toggleClass('active');
       $('body').toggleClass('simplenav-push-toright');
   });
    $('#navigation .avatar').hover(function(){
        clearTimeout(hide);
        $('.hovernav').addClass('active');
    }, function(){
        /* I use a timeout to prevent the menu from hiding while the mouse is going to the menu. like jQuery hoverIntent */

        hide = setTimeout(function(){
            $('.hovernav').removeClass('active');
        }, 500);
    });
});
